<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\Controller as Conn;
use Session;
use DateTime;   

class DashboardController extends Controller
{
    public function dashboard(Request $request)
    {
        $url    = 'https://data.petabencana.id/reports/archive';
        $params = [
            'start'     => '2017-12-04T00:00:00+0700',
            'end'       => '2017-12-06T05:00:00+0700',
            'geoformat' => 'geojson'
        ];

        $responseCurl = Curl::to($url)
                        ->withData($params)
                        ->get();

        // $data = json_decode($responseCurl);
        // return $responseCurl;
        

        // foreach($data->data->merchants as $item){
        //     $item->amount = $this->rupiah($item->amount);
        // }

        // $response = [   
        //     'status'    => true, 
        //     'message'   => 'Success',
        //     'data'      => $data,
        // ];

        return view('dashboard')
                ->with('data', $responseCurl);
    }

    public function getData(){
        $url    = 'https://data.petabencana.id/reports/archive';
        $params = [
            'start'     => '2017-12-04T00:00:00+0700',
            'end'       => '2017-12-06T05:00:00+0700',
            'geoformat' => 'geojson'
        ];

        $responseCurl = Curl::to($url)
                        ->withData($params)
                        ->get();
        
        return $responseCurl;
    }

    private function rupiah($value)
    {
        $result = "Rp " . number_format($value,2,',','.');
	    return $result;
    }
}
