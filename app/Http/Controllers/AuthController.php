<?php

namespace App\Http\Controllers;
use Redirect;
use Validator, Hash;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Auth;

use App\Entities\User;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function login(Request $request){
        $user = $request->session()->get('user');
        // return response()->json($user,200);
        if($user){
            return Redirect::to('dashboard');
        }else{
            return view('login');
        }
    }

    public function doLogin(Request $request){
        $validated = $request->validate([
            'username' => 'required',
            'password' => 'required',
            'phone'    => 'required|regex:/(08)[0-9]{9}/',
        ]);

        if($validated){
            $credentials = $request->only('username', 'password','phone');
            if ($token = auth()->attempt($credentials)) {
                $user = User::where('username', $request->username)->first();
                $request->session()->put('user', $user);
                $request->session()->put('token', $token);
    
                return Redirect::to('dashboard');
            }else{
                return  redirect()->back()
                                ->withInput($request->input())
                                ->with('error','Username or Password or Phone did not match or user does not exist!');
            }
        }
    }

    public function register(Request $request){
        return view('signUp');
    }

    public function doRegister(Request $request){
        $validated = $request->validate([
            'username' => 'required|unique:users',
            'password' => 'required|confirmed',
            'phone'    => 'required|regex:/(08)[0-9]{9}/',
        ]);

        if($validated){
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->phone    = $request->phone;
            $user->save();
            
            return Redirect::to('login');
        }else{
            return Redirect::to('register')
                            ->withInput();
        }
    }

    public function logout(Request $request){
        $request->session()->forget('user');
        $request->session()->forget('token');
        return Redirect::to('login');
    }
}
