<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Hash;
use Redirect;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Ixudra\Curl\Facades\Curl;

use App\Entities\User;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserValidator
     */
    protected $validator;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $data = User::select('*');
    
        if($request->has('limit')){
            $data->take($request->get('limit'));
            
            if($request->has('offset')){
            	$data->skip($request->get('offset'));
            }
        }

        if($request->has('order_type')){
            if($request->get('order_type') == 'asc'){
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'));
                }else{
                    $data->orderBy('created_at');
                }
            }else{
                if($request->has('order_by')){
                    $data->orderBy($request->get('order_by'), 'desc');
                }else{
                    $data->orderBy('created_at', 'desc');
                }
            }
        }else{
            $data->orderBy('created_at', 'desc');
        }

        $data = $data->get();
        // var_dump($data);exit();
        return view('users.list')
                ->with('data', $data); 
    }

    public function create(Request $request){
        return view('users.add');
    }

    public function store(UserCreateRequest $request)
    {
        $validated = $request->validate([
            'username' => 'required|unique:users',
            'password' => 'required|confirmed',
            'phone'    => 'required|regex:/(08)[0-9]{9}/',
        ]);

        if($validated){
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->phone    = $request->phone;
            $user->save();
            
            return Redirect::to('user');
        }else{
            return Redirect::to('user_create')
                            ->withInput();
        }
    }

    public function edit($id){
        $user = User::find($id);
        return view('users.edit')
                ->with('data', $user);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $validated = $request->validate([
            'password' => 'confirmed',
            'phone'    => 'required|regex:/(08)[0-9]{9}/',
        ]);

        if($validated){
            $user = User::find($id);
            if($user){
                $user->name     = $request->name;
                if($user->username != $request->username){
                    $check = User::where('username', $request->username)->first();
                    if($check){
                        return  redirect()->back()
                                ->with('error','The username has already been taken.');
                    }
                    $user->username = $request->username;
                }
                $user->password = Hash::make($request->password);
                $user->phone    = $request->phone;
                $user->save();
            }
            
            return Redirect::to('user');
        }else{
            return  redirect()->back()
                                ->withInput($request->input());
        }
    }

    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        if($deleted){
            return Redirect::to('user');
        }else{
            return  redirect()->back()
                                ->with('error', 'Data failed to deleted');
        }
    }
}
