<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class CheckSessionUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->session()->get('user');
        if($user){
            return $next($request);
        }else{
            return Redirect::route('login');
        }
    }
}
