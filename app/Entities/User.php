<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Webpatser\Uuid\Uuid;
use Auth;

/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable; 
    use TransformableTrait;
    use SoftDeletes;

    protected $presenter = UserPresenter::class;
    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'username',
        'password',
        'phone'
    ];

    protected $table = 'users';
    protected $hidden = [
        'password'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
