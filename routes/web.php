<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login', 'AuthController@doLogin')->name('do_login');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/register', 'AuthController@doRegister')->name('do_register');

Route::group(['middleware' => ['check.session']], function() {
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');
    Route::get('/user', 'UsersController@index')->name('user');
    Route::get('/user/create', 'UsersController@create')->name('user_create');
    Route::post('/user', 'UsersController@store')->name('user_store');
    Route::get('/user/{id}/edit', 'UsersController@edit')->name('user_edit');
    Route::post('/user/{id}', 'UsersController@update')->name('user_update');
    Route::delete('/user/{id}/delete', 'UsersController@destroy')->name('user_delete');
    Route::get('/getData','DashboardController@getData')->name('getData');
});

