<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ request()->is('dashboard') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('dashboard')}}">
                <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/dashboard.png')}}" alt="">
                    <span class="nav-text">Dashboard</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->is('user') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('user')}}">
                    <img class="img_bintang_sidebar" src="{{asset('/assets/images/icon_bintang/agent.png')}}" alt="">
                    <span class="nav-text">Users</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->