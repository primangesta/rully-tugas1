@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb">
                <h1>Dashboard</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="flex_bintang">
                <div class="col-lg-12 col-md-12 flex_bintang_row padding_lr_0">
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div id="mapid"></div>
                    </div>
                </div>
            </div>

@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>
     <script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
     <script>
        $(document).ready(function() {
            var url = "{{URL('getData')}}";
            $.ajax({
                url: url,
                type: "GET",
                data:{ 
                    _token:'{{ csrf_token() }}'
                },
                cache: false,
                dataType: 'json',
                success: function(dataResult){
                    var mymap = L.map('mapid').setView([-0.7864916, 119.3924473], 4);
                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(mymap);    

                    var resultData = dataResult['result']['features'];
                    $.each(resultData,function(index,row){
                        var geometry    = row['geometry']['coordinates'];
                        var properties  = row['properties'];
                        L.circle([geometry[1], geometry[0]], 500, {
                            color: 'red',
                            fillColor: '#f03',
                            fillOpacity: 0.5
                        }).addTo(mymap).bindPopup("<b>Coor</b> : "+geometry[1]+","+geometry[0]+"</b><br>"+
                                                "<b>Date</b> : "+properties['created_at']+"<br>"+
                                                "<b>Disaster Type</b> : "+properties['disaster_type']+"<br>"+
                                                "<b>Source</b> : "+properties['source']+"<br>"+
                                                "<b>Status</b> : "+properties['status']

                        );
                        
                    });
                }
            });
        });
     </script>
     
     
@endsection
