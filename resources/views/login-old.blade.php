<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Tugas 1</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-blue.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/custom/bintang.css')}}">
    </head>

    <body>
        <div class="auth-layout-wrap align-items-md-end" style="background-image: url({{asset('assets/images/icon_bintang/bcg_signin3.svg')}})">
            <div class="auth-content width_signin m-0">
                <div style=" border-radius: 0px;" class="card o-hidden h-100vh border_radius_0 justify-content-center">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="p-4">
                                <div class="auth-logo text-center mb-4">
                                    <img class="logo_bintang_signin" src="{{asset('/assets/images/icon_bintang/selada-transparant-final.png')}}" alt="">
                                </div>
                                <h1 class="mb-3 text-18">Sign In</h1>
                                <form method="POST" action="{{ route('do_login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input id="username"
                                            class="form-control form-control-rounded @error('username') is-invalid @enderror"
                                            name="username" value="{{ old('username') }}" required autocomplete="username"
                                            autofocus>
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password"
                                            class="form-control form-control-rounded @error('password') is-invalid @enderror"
                                            name="password" required autocomplete="current-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="telp">Phone Number</label>
                                        <input id="telp"
                                            class="form-control form-control-rounded @error('telp') is-invalid @enderror"
                                            name="telp" required autocomplete="current-telp">
                                        @error('telp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <button class="btn btn-rounded btn-primary btn_selada btn-block mt-4 mb-3 p-2">Sign In</button>

                                </form>
                                <div class="mt-3 text-center">

                                    <a href="{{ route('register') }}" class="text-muted"><u>Register</u></a>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>

        <script src="{{asset('assets/js/script.js')}}"></script>
    </body>

</html>