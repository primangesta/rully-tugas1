@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
  <div class="breadcrumb">
        <h1>User</h1>
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">
                <div class="card-body">
                    @if(Session('message'))
                        <div class="alert alert-success" role="alert">
                            <strong class="text-capitalize">Success : </strong> {{ Session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <!-- <h4 class="card-title mb-3">User List</h4> -->
                    <div class="flex_bintang spaceb_bintang">
                        <h4 class="card-title mb-3">List User</h4>
                        <a href="{{route('user_create')}}">
                            <button  type="button" class="btn btn-warning btn-icon mb-3">
                                <span class="ul-btn__text">Add User</span>
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Phone Number</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($data as $item)
                                <tr>
                                    <th scope="row">{{ $no }}</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->username}}</td>
                                    <td>{{$item->phone}}</td>
                                    <td>
                                        <a href="{{route('user_edit', $item->id)}}" class="text-success">
                                            <abbr title="Edit Data"><button class="btn btn-primary btn-sm m-1">Edit</button></abbr>
                                        </a>
                                        <form method="POST" action="{{ route('user_delete', $item->id) }}" style="display:contents">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <abbr title="Edit Data"><button type="submit" class="btn btn-danger btn-sm m-1 show_confirm">Delete</button></abbr>
                                        </form>
                                    </td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of col -->
    </div>
    <!-- end of row -->

@endsection

@section('page-js')

<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script type="text/javascript">
    $('.show_confirm').click(function(e) {
        if(!confirm('Are you sure you want to delete this?')) {
            e.preventDefault();
        }
    });
</script>

@endsection

