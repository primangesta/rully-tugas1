@extends('layouts.master')
@section('main-content')
    <div class="breadcrumb">
                <h1>Edit User</h1>
            </div>
            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-5">
                        <div class="card-body">
                            <form action="{{route('user_update', $data->id)}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="username">Name</label>
                                    <input id="name" type="text"
                                        class="form-control-rounded form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ $data->name }}" required autocomplete="name"
                                        autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input id="username" type="username"
                                        class="form-control-rounded form-control @error('username') is-invalid @enderror"
                                        name="username" value="{{ $data->username }}" required autocomplete="username">

                                    @if(Session('error'))
                                        <span class="invalid-feedback" role="alert" style="display: contents;">
                                            <strong>{{ Session('error') }}</strong>
                                        </span>
                                    @endif

                                    @error('username')
                                    asdasd
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password"
                                        class="form-control-rounded form-control @error('password') is-invalid @enderror"
                                        name="password" autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="repassword">Retype password</label>
                                    <input id="password-confirm" type="password"
                                        class="form-control-rounded form-control" name="password_confirmation"
                                     autocomplete="new-password">
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input id="phone" type="text"
                                        class="form-control-rounded form-control @error('phone') is-invalid @enderror"
                                        name="phone" value="{{ $data->phone }}" required autocomplete="phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="row" style="margin-left: 0px;">
                                <a href="{{route('user')}}"><button type="button" style="width:100px; margin-right:5px;" class="btn btn-primary btn-block btn-rounded mt-3">Back</button></a>
                                <button type="submit" style="width:100px" class="btn btn-primary btn-block btn-rounded mt-3">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


@endsection

@section('page-js')

@endsection

@section('bottom-js')

    <script src="{{asset('assets/js/form.validation.script.js')}}"></script>

@endsection
